const tools = require("./tools")

test("generate triangular numbers", () => {
    const trian = tools.generateTriangular()
    const sample = [1, 3, 6, 10, 15, 21, 28]

    sample.forEach((item) => {
        expect(trian.next().value).toBe(item)
    })
});

test("test primChecker function", () => {
    const sample = [
        {value: 2, result: true},
        {value: 3, result: true},
        {value: 4, result: false},
        {value: 5, result: true},
        {value: 6, result: false},
        {value: 7, result: true},
        {value: 8, result: false},
        {value: 9, result: false},
        {value: 10, result: false},
        {value: 11, result: true},
        {value: 12, result: false},
        {value: 13, result: true},
        {value: 14, result: false},
    ]

    sample.forEach((item) => {
        expect(tools.primChecker(item.value)).toBe(item.result)
    })
})

test("test prim number generator", () => {
    const prim = tools.generatePrim()

    const sample = [2, 3, 5, 7, 11, 13, 17, 19, 23]
    sample.forEach((item) => {
        expect(prim.next().value).toBe(item)
    })

})

test("test getPrimeByNTH", () => {
    const sample = [
        {value: 0, result: 2},
        {value: 2, result: 5},
        {value: 4, result: 11},
        {value: 6, result: 17},
        {value: 8, result: 23},
        {value: 2, result: 5},
        {value: 45, result: 199},
    ]

    sample.forEach((item) => {
        expect(tools.getPrimeByNTH(item.value)).toBe(item.result)
    })
})

test("test primeFactorization", () => {
    const sample = [
        {value: 2, result: [2]},
        {value: 3, result: [3]},
        {value: 4, result: [2,2]},
        {value: 6, result: [2,3]},
        {value: 9, result: [3,3]},
        {value: 255, result: [3,5,17]},
        {value: 28, result: [2,2,7]},
    ]

    sample.forEach((item) => {
        expect(tools.primeFactorization(item.value)).toStrictEqual(item.result)
    })
})
