
function* generateTriangular() {
    counter = 0;
    num = 0;
    while (true) {
        counter++;
        num += counter;
        yield num;
    }
}


function* generatePrim() {
    num = 2;
    yield num;
    while(true) {
        while(true) {
            num++;
            if (primChecker(num)) break;
        }
        yield num;
    }
}


function primChecker(x) {
    for (let i=2; i<x; i++)
        if (x % i == 0) return false;
    return true;
}


var cache = []
const prim = generatePrim()

function getPrimeByNTH(i) {
    while (cache.length <= i ) {
        cache.push(prim.next().value)
    }
    return cache[i]
}


function primeFactorization(num) {
    divides = []
    var i = 0
    while(true) {
        var d = getPrimeByNTH(i)
        i++
        if (num % d == 0) {
            divides.push(d)
            num = num / d
            i = 0
            console.log(num)
        }
        if (num == 1) break;
    }

    return divides
}

module.exports = { generateTriangular, generatePrim, primChecker, getPrimeByNTH, primeFactorization }
