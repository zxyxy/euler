#include <stdio.h>
#include "main.h"
#include "func.h"


int main(void)
{
  unsigned long counter = 2;
  unsigned long num = 0;
  while (1) {
    num = next_triangular();
    if (get_dividers(num) >= 500) {
      break;
    }

    if (counter % 1000 == 0) {
      printf("- %lu ---- %lu \n", num, get_dividers(num));
    }
    ++counter;
  }
  printf("%lu is the number\n", num);
}
