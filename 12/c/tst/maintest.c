#include <glib.h>
#include <stdio.h>
#include "func.h"


static void test_generate_triangular()
{
  unsigned long sample[] = {1, 3, 6, 10, 15, 21, 28};
  int arrLen = sizeof sample / sizeof sample[0];

  for (int i = 0; i < arrLen; ++i) {
    g_assert(next_triangular()==sample[i]);
  }
}


static void test_dividers()
{
  unsigned long sample[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
  unsigned long res[] = {1, 2, 2, 3, 2, 4, 2, 4, 3, 4, 2, 6};

  int arrLen = sizeof sample / sizeof sample[0];

  for (int i = 0; i < arrLen; ++i) {
    g_assert(get_dividers(sample[i])==res[i]);
  }
}


int main(int argc, char **argv)
{
  g_test_init(&argc, &argv, NULL);
  g_test_add_func("/test_generate_triangular", test_generate_triangular);
  g_test_add_func("/test_dividers", test_dividers);

  return g_test_run();
}
