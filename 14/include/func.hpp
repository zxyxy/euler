#include <vector>

#ifndef FUNC_HPP
#define FUNC_HPP

#define ULONG unsigned long


std::vector<ULONG> collatz_sequence(const ULONG start);

#endif
