#include <glib.h>
#include <stdio.h>
#include "func.hpp"
#include <vector>

#define ULONG unsigned long

static void test_collatz_sequence()
{
  const std::vector<ULONG> tester = std::vector<ULONG>{13, 40, 20, 10, 5, 16, 8, 4 , 2, 1};
  const auto sample = collatz_sequence(13);

  g_assert(sample.size()==10);
  g_assert(sample.size()==tester.size());
  g_assert(sample==tester);
}

static void test_collatz_sequence_2()
{
  const std::vector<ULONG> tester = std::vector<ULONG>{10, 5, 16, 8, 4 , 2, 1};
  const auto sample = collatz_sequence(10);

  g_assert(sample.size()==tester.size());
  g_assert(sample==tester);
}


int main(int argc, char **argv)
{
  g_test_init(&argc, &argv, NULL);
  g_test_add_func("/test_collatz_sequence", test_collatz_sequence);
  g_test_add_func("/test_collatz_sequence_2", test_collatz_sequence_2);

  return g_test_run();
}
