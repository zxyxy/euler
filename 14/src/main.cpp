#include <stdio.h>
#include "main.hpp"
#include "func.hpp"
#include <iostream>

int main(void)
{
  ULONG max = 1;
  ULONG startnum = 0;
  for (ULONG i = 10; i< 1000000; ++i) {
    std::vector<ULONG> list = collatz_sequence(i);
    if (max < list.size()) {
      max = list.size();
      startnum = list[0];
    }
  }

  std::cout << "the longest is :" << startnum << std::endl;

}
