#include "func.hpp"

std::vector<ULONG> collatz_sequence(const ULONG start) {
    std::vector<ULONG> res;
    res.push_back(start);
    ULONG x = start;
    while (x != 1) {
        if (x % 2 == 0) {
            x = x/2;
        } else {
            x = 3 * x + 1;
        }
        res.push_back(x);
    }

    return res;
}
