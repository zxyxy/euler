
package main

import (
	"testing"
)

func TestMulti(t *testing.T) {
	matrix := [][]int{
		{1,2,3,4,5,6,7},
		{1,2,3,4,5,6,7},
		{1,2,3,4,5,6,7},
		{1,2,3,4,5,6,7},
		{1,2,3,4,5,6,7},
		{1,2,3,4,5,6,7},
	}

	test := [][]int{
		{0,0,1,1,24},
		{1,0,1,1,120},
		{0,0,1,0,24},
		{0,0,0,1,1},
		{5,0,-1,1,360},
	}

	for _, row := range test {
		res, err := Multi(matrix,row[0], row[1], row[2], row[3])

		if res != row[4] && err != nil {
			t.Logf("Wrong res: %d should be: %d", res, row[4])
			t.Fail()
		}
	}
}

func TestMultiOutOfRAnge(t *testing.T) {
	matrix := [][]int{
		{1,2,3,4,5,6,7},
		{1,2,3,4,5,6,7},
		{1,2,3,4,5,6,7},
		{1,2,3,4,5,6,7},
		{1,2,3,4,5,6,7},
		{1,2,3,4,5,6,7},
	}

	test := [][]int{
		{5,0,1,0,0},
		{0,5,0,1,0},
		{1,0,-1,0,0},
		{0,1,0,-1,0},
	}
	for _, row := range test {
		_, err := Multi(matrix,row[0], row[1], row[2], row[3])

		if err == nil {
			t.Logf("Should return error, test row: %d", row)
			t.Fail()
		}
	}
}
